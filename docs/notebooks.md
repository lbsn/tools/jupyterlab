# Example Notebooks

## Workshop Series TUD

The [Social Media, Jupyter Lab & Tag Maps Workshop Series at TUD](https://gitlab.hrz.tu-chemnitz.de/tud-ifk/jupyter_python_datascience) contains a number of example Jupyter Notebooks that can be run with Carto-Lab Docker.

**HTML Versions of Notebooks:**

1. [01_raw_intro.html](https://kartographie.geo.tu-dresden.de/ad/jupyter_python_datascience/01_raw_intro.html)
2. [02_hll_intro.html](https://kartographie.geo.tu-dresden.de/ad/jupyter_python_datascience/02_hll_intro.html)
3. [03_tagmaps.html](https://kartographie.geo.tu-dresden.de/ad/jupyter_python_datascience/03_tagmaps.html)
4. [04_topic_classification.html](https://kartographie.geo.tu-dresden.de/ad/jupyter_python_datascience/04_topic_classification.html)

## Publications

The following publications include Jupyter notebooks created with Carto-Lab Docker as supplementary material:

- **Dunkel, A., & Burghardt, D. (2024).** Assessing Perceived Landscape Change from Opportunistic Spatiotemporal Occurrence Data. Land, 13(7), 1091. [DOI](https://doi.org/10.3390/land13071091)
    - S1 Jupyter Notebook: [01_mass_invasion.html](https://code.ad.ioer.info/wip/temporal_landscapes/html/01_mass_invasion.html)
    - S2 Jupyter Notebook: [02_reddit_api.html](https://code.ad.ioer.info/wip/temporal_landscapes/html/02_reddit_api.html)
    - S3 Jupyter Notebook: [03_reddit_pmaw.html](https://code.ad.ioer.info/wip/temporal_landscapes/html/03_reddit_pmaw.html)
    - S4 Jupyter Notebook: [04_reddit_privacy.html](https://code.ad.ioer.info/wip/temporal_landscapes/html/04_reddit_privacy.html)
    - S5 Jupyter Notebook: [05_reddit_vis.html](https://code.ad.ioer.info/wip/temporal_landscapes/html/05_reddit_vis.html)
    - S6 Jupyter Notebook: [06_cherry_blossoms.html](https://code.ad.ioer.info/wip/temporal_landscapes/html/06_cherry_blossoms.html)
    - S7 Jupyter Notebook: [07_hotspots.html](https://code.ad.ioer.info/wip/temporal_landscapes/html/07_hotspots.html)
    - S8 Jupyter Notebook: [08_milvus_conversion.html](https://code.ad.ioer.info/wip/temporal_landscapes/html/08_milvus_conversion.html)
    - S9 Jupyter Notebook: [09_milvus_maps.html](https://code.ad.ioer.info/wip/temporal_landscapes/html/09_milvus_maps.html)
    - S10 Jupyter Notebook: [10_milvus_chi.html](https://code.ad.ioer.info/wip/temporal_landscapes/html/10_milvus_chi.html)
- **Dunkel, A., Burghardt, D., & Gugulica, M. (2024).** Generative Text-to-Image Diffusion for Automated Map Production Based on Geosocial Media Data. KN - Journal of Cartography and Geographic Information. [DOI](https://doi.org/10.1007/s42489-024-00159-9)
    - [Notebook 1](https://kartographie.geo.tu-dresden.de/ad/wip/mapnik_stablediffusion/html/01_tagmaps.html): Data preparation, spatial tag clustering (Tagmaps)
    - [Notebook 2](https://kartographie.geo.tu-dresden.de/ad/wip/mapnik_stablediffusion/html/02_generativeai.html): Generative AI parameters & tests
    - [Notebook 3](https://kartographie.geo.tu-dresden.de/ad/wip/mapnik_stablediffusion/html/03_map_processing.html): Map generation
- **Dunkel, A., Hartmann, M. C., Hauthal, E., Burghardt, D., & Purves, R. S. (2023).** From sunrise to sunset: Exploring landscape preference through global reactions to ephemeral events captured in georeferenced social media. PLOS ONE, 18(2), e0280423. [DOI](https://doi.org/10.1371/journal.pone.0280423)
    - [01_grid_agg.html](https://ad.vgiscience.org/sunset-sunrise-paper/01_grid_agg.html)
    - [02_visualization.html](https://ad.vgiscience.org/sunset-sunrise-paper/02_visualization.html)
    - [03_chimaps.html](https://ad.vgiscience.org/sunset-sunrise-paper/03_chimaps.html)
    - [04_combine.html](https://ad.vgiscience.org/sunset-sunrise-paper/04_combine.html)
    - [05_countries.html](https://ad.vgiscience.org/sunset-sunrise-paper/05_countries.html)
    - [06_semantics.html](https://ad.vgiscience.org/sunset-sunrise-paper/06_semantics.html)
    - [07_time.html](https://ad.vgiscience.org/sunset-sunrise-paper/07_time.html)
    - [08_relationships.html](https://ad.vgiscience.org/sunset-sunrise-paper/08_relationships.html)
    - [09_statistics.html](https://ad.vgiscience.org/sunset-sunrise-paper/09_statistics.html)
- **Dunkel, A., Löchner, M., & Burghardt, D. (2020).** Privacy-Aware Visualization of Volunteered Geographic Information (VGI) to Analyze Spatial Activity: A Benchmark Implementation. ISPRS International Journal of Geo-Information, 9(10), 607. [DOI](https://doi.org/10.3390/ijgi9100607)
    - [01_preparations.html](http://ad.vgiscience.org/yfcc_gridagg/01_preparations.html)
    - [02_yfcc_gridagg_raw.html](http://ad.vgiscience.org/yfcc_gridagg/02_yfcc_gridagg_raw.html)
    - [03_yfcc_gridagg_hll.html](http://ad.vgiscience.org/yfcc_gridagg/03_yfcc_gridagg_hll.html)
    - [04_interpretation.html](http://ad.vgiscience.org/yfcc_gridagg/04_interpretation.html)