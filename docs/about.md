Carto-Lab Docker is maintained by Alexander Dunkel and [vgiscience.org](https://vgiscience.org)

Found any errors or bugs? Please email me mail[ät]alexanderdunkel.com

Carto-Lab Docker docs built with [mkdocs](https://github.com/mkdocs/mkdocs) and [ReadTheDocs theme](https://github.com/mkdocs/mkdocs/tree/master/mkdocs/themes/readthedocs).

Carto-Lab Docker is developed under open source GNU GPLv3 License.