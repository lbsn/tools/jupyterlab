# R

The `:rstudio` version of CartoLab-Docker contains a number of base packages for mapping in R.

See the `/rstudio/environment.r.yml` file for the list of packages available in the `R`-env.

```yaml
{!../rstudio/environment_r.yml!}
```

