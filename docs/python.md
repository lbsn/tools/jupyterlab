# Python packages

All versions of CartoLab-Docker contain a `worker_env` with a number of base packages for mapping in Python.

See the `environment_default.yml` file for the list of packages available in the `worker_env`.

```yaml
{!../environment_default.yml!}
```
