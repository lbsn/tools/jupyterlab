# Quick-Start


Where you start depends on how Carto-Lab Docker was setup:

1. **Using the docker container**
   If you start from scratch, Carto-Lab Docker must be started somewhere. This
   can either be your own computer or a Virtual Machine (VM) to which you connect
   via SSH-Tunnel or through a web URL. See the Section on [Docker Container Setup](/docker/)
   for guides to setup the container.

2. **Using JupyterLab**
   If Carto-Lab Docker is available through a URL or hosted instance, directly
   jump to the next section on how to use [JupyterLab](/jupyter/).



